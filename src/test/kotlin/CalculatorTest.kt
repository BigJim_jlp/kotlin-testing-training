import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CalculatorTest {

    @Test
    fun `empty string should return zero`() {
        val result = add("")
        assertEquals(result, 0)
    }

    @Test
    fun `single number should rerurn that number`() {
        val result = add("7")
        assertEquals(result, 7)
    }

    @Test
    fun `2,3 should sum to 5`() {
        val result = add("2,3")
        assertEquals(result, 5)
    }

    @Test
    fun `2,3,5 should sum to 10 `() {
        val result = add("2,3,5")
        assertEquals(result, 10)
    }

    @Test
    fun `2|3|5 should sum to 10 `() {
        val result = add("2|3|5")
        assertEquals(result, 10)
    }
}
