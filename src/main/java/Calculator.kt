fun add(numbers: String): Int {
    var total = 0
    if (numbers == "") {
        return 0
    }
    val numberList = numbers.split(",","|")
    if (numberList.isNotEmpty()) {
        for (number in numberList) {
            total += number.toInt()
        }
    }
    return total
}